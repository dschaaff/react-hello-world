# React Hello World Pipeline

Why does this exist?

- my first walkthrough of creating a hello world react app
- demonstrating hosting a react app with s3 and cloudfront
- for me to experiment with gitlab ci pipelines

## Terraform

The terraform code in the `./terraform` dir creates the AWS resources. In a production scenario this code
would be abstracted into a terraform module in addition to storing the state in s3 with dynamodb locking.

## Gitlab CI

The CI pipeline goes through the following steps

- runs tests
- builds the app with weback
- on the master branch, copies app to s3
- on the master branch, invalidates cloudfront cache for index.html file