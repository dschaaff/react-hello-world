import React from 'react';
import ReactDOM from 'react-dom';

const App = () => (
  <div>
    <h1>Hello World App</h1>
    Hello from the example branch!
    </div>
)


ReactDOM.render(
  <App />,
  document.getElementById('root')
)